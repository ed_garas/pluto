from django.contrib import admin
from .models import *


class PlanAdmin(admin.ModelAdmin):
    list_display = ('name', 'price_currency', 'file_limit', 'file_limit_readable')

    def price_currency(self, obj):
        return ("%s EU" % (obj.price))

    price_currency.short_description = 'Price'


admin.site.register(Plan, PlanAdmin)