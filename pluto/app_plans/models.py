from django.db import models
from pluto_project.helpers import *

class Plan(models.Model):
    name        = models.CharField(max_length=120)
    description = models.TextField(blank=True)
    price       = models.IntegerField(blank=True)
    file_limit  = models.IntegerField()

    class Meta:
        verbose_name_plural = 'plans'

    def __str__(self):
        return self.name

    @property
    def file_limit_readable(self):
        return human_readable_size(self.file_limit, 2)