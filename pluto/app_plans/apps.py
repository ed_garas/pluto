from django.apps import AppConfig


class AppPlansConfig(AppConfig):
    name = 'app_plans'
    verbose_name = 'Plans'
