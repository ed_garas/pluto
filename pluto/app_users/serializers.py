from rest_framework import serializers
from django.contrib.auth import password_validation
from .models import *



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'full_name',
            'email',
            'password',
            'avatar',
            'is_active',
            'date_joined',
            'plan',
            'space',
            'space_readable',
            'space_left',
            'has_space',
        )
        read_only_fields = ('is_staff', 'is_superuser', 'is_active', 'date_joined',)

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance



class PendingUserSerializer(serializers.ModelSerializer):

    def validate_password(self, value):
        password_validation.validate_password(value, self.instance)
        return value

    class Meta:
        model = PendingUser
        fields = (
            'id',
            'email',
            'full_name',
            'password',
            'email',
            'confirm_token',
        )
        read_only_fields = ('id', 'date_joined',)