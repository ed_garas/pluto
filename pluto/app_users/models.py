from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from app_plans.models import Plan
import string, random
from pluto_project.helpers import *

def user_avatar_path(instance, filename):
    return "user_{id}/avatar/{file}".format(id=instance.id, file=filename)

def project_images_path(instance, filename):
    return "user_{id}/project_{project}/{file}".format(id=instance.project.user.id, project=instance.project.id, file=filename)

def randomSlug():
    string_slug = string.ascii_lowercase + string.digits
    return ''.join(random.choice(string_slug) for i in range(random.randint(5,30)))


class UserManager(BaseUserManager):
    def create_user(self, email, password, **kwargs):
        user = self.model(
            email=self.normalize_email(email),
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        user = self.model(
            email=email,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'email'
    email       = models.EmailField(unique=True)
    full_name   = models.CharField(max_length=255, blank=True, null=True)
    avatar      = models.ImageField(blank=True, null=True, upload_to=user_avatar_path)
    plan        = models.ForeignKey(Plan, blank=True, null=True)
    is_active   = models.BooleanField(default=False)
    is_staff    = models.BooleanField(default=False)
    date_joined = models.DateField(auto_now_add=True)
    objects     = UserManager()

    @property
    def project_number(self):
        projects = self.project_set.all()
        return projects.count()

    @property
    def space(self):
        projects = self.project_set.all()
        space = 0
        for project in projects:
            space += project.space
        return space

    @property
    def space_readable(self):
        return human_readable_size(self.space, 2)

    @property
    def space_left(self):
        if self.plan:
            return human_readable_size(self.plan.file_limit - self.space, 2)
        return 0

    @property
    def has_space(self):
        if self.plan:
            return self.plan.file_limit >= self.space
        return False


    def get_full_name(self):
        return self.email
    def get_short_name(self):
        return self.email



class PendingUser(models.Model):
    email           = models.EmailField(max_length=255, unique=True)
    full_name       = models.CharField(max_length=255, blank=True, null=True)
    password        = models.CharField(max_length=255)
    confirm_token   = models.CharField(max_length=255, default=randomSlug)
    is_email_send   = models.BooleanField(default=False)
    date_joined     = models.DateField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.pk:  # this will ensure that the object is new
            self.password = make_password(self.password)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'pending users'

    def __str__(self):
        return self.email