from rest_framework.generics import get_object_or_404
from rest_framework.permissions import *
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import *
from .emails import *


@api_view( ('GET',) )
def remove_token(request, format=None):
    if request.method == 'GET':
        request.user.auth_token.delete()
        res = {
            'status': True,
            'message': 'token removed',
        }
        return Response(res, status=status.HTTP_200_OK)



@api_view( ('GET','PUT','DELETE',) )
def user_profile(request, format=None):

    try:
        user = User.objects.get(pk=request.user.id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response({'data': serializer.data})

    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'data': serializer.data})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        user.delete()
        return Response({'detail': 'deleted'}, status=status.HTTP_204_NO_CONTENT)



@api_view( ('POST',) )
@permission_classes( (AllowAny,) )
def create_user(request):
    """
    Create new user
    required fiels:
    1. email
    2. password
    """
    if request.method == 'POST':
        request_email = request.data['email']

        if User.objects.filter(email=request_email).exists():
            return Response({'status': 'user with this email already exist'}, status=status.HTTP_400_BAD_REQUEST)

        serializer = PendingUserSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        args = {
            'token': serializer.data['confirm_token'],
            'request': request,
        }
        email = Email(request_email, args)
        is_send = email.sendConfirmEmail()

        if is_send:
            PendingUser.objects.filter(id=serializer.data['id']).update(is_email_send=True)

        data = {
            'message': 'email with confirmation link is send to: ' + request_email,
            'data': serializer.data
        }
        return Response(data, status=status.HTTP_201_CREATED)


@api_view( ('GET',) )
@permission_classes( (AllowAny,) )
def user_confirm(request, confirm_token):

    if request.method == 'GET':
        pending_user = get_object_or_404(PendingUser, confirm_token=confirm_token)

        if not User.objects.all().filter(email=pending_user.email).exists():
            User.objects.create(
                email=pending_user.email,
                password=pending_user.password,
                full_name=pending_user.full_name,
                is_active=True
            )
            pending_user.delete()
            return Response({'confirmed': True})

        return Response({'confirmed': False, 'message': 'user already exist'})