from django.contrib import admin
from .models import *
from django.contrib.auth import get_user_model

class PendingUserAdmin(admin.ModelAdmin):
    list_display = ('email', 'date_joined')


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'project_number', 'space_readable',)

admin.site.register(PendingUser, PendingUserAdmin)
admin.site.register(get_user_model(), UserAdmin)