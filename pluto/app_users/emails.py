import socket
from django.core.mail import send_mail
from django.http import HttpRequest
from django.template.loader import render_to_string
from rest_framework.reverse import reverse


class Email():

    def __init__(self, send_to_email, args):
        self.send_to_email = send_to_email
        self.args = args


    def sendConfirmEmail(self):

        data = {
            'url_with_token': reverse('register_confirm', args=[self.args['token']], request=self.args['request']),
            'to_email': self.send_to_email
        }

        email_template = render_to_string('email/confirm_account.html', data)
        is_send = send_mail(
            'Pluto display: Confirm Account',
            'Here is the message.',
            'hello@pluto.com',
            [self.send_to_email],
            html_message=email_template,
            fail_silently=False,
        )
        return is_send