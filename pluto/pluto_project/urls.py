from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns

from .views import *
from app_projects.views import *
from app_users.views import *

from rest_framework.authtoken import views
from rest_framework.routers import SimpleRouter

# router = SimpleRouter()
# router.register(r'images', ImageViewSet, 'images')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),

    # url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/auth/logout/', remove_token, name="logout"),
    url(r'^api/v1/auth/login/', views.obtain_auth_token, name="login"),
    url(r'^api/v1/auth/register/$', create_user, name="register"),
    url(r'^register/confirm/(?P<confirm_token>[\w-]+)$', user_confirm, name="register_confirm"),
    url(r'^api/v1/profile/$', user_profile, name='profile'),
    url(r'^api/v1/projects/$', project_list, name='projects_list'),
    url(r'^api/v1/projects/(?P<project_id>[0-9]+)$', projects_detail, name="projects_detail"),
    url(r'^api/v1/projects/(?P<project_id>[0-9]+)/images$', image_list, name="project_images"),
    url(r'^api/v1/projects/(?P<project_id>[0-9]+)/images/(?P<image_id>[0-9]+)$', image_detail, name="project_images_detail"),

    url(r'^p/(?P<slug>[\w-]+)$', public_project, name="public_project"),
    url(r'^i/(?P<slug>[\w-]+)$', public_image, name="public_image"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns = format_suffix_patterns(urlpatterns)