import socket
from django.core.mail import send_mail
from django.http import HttpRequest
from django.template.loader import render_to_string
from rest_framework.reverse import reverse


class AccountConfirmEmail():

    def __init__(self, request, to_email, token):
        self.request = request
        self.to_email = to_email
        self.token = token


    def send(self):

        data = {
            'url_with_token': reverse('register_confirm', args=[self.token], request=self.request),
            'to_email': self.to_email
        }

        msg_html = render_to_string('email/confirm_account.html', data)
        is_send = send_mail(
            'Pluto display: Confirm Account',
            'Here is the message.',
            'hello@pluto.com',
            [self.to_email],
            html_message=msg_html,
            fail_silently=False,
        )
        return is_send