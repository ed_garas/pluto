from rest_framework import serializers
from .models import *
from pluto_project.helpers import *


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = (
            'id',
            'name',
            'slug',
            'active',
            'user',
            'images_count',
            'space',
            'space_readable',
            'created_at',
            'updated_at',
        )
        read_only_fields = ('id', 'space', 'space_readable', 'images_count', 'created_at', 'updated_at',)


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = (
            'id',
            'name',
            'slug',
            'image',
            'size',
            'size_readable',
            'screen_type',
            'project',
            'active',
            'created_at',
            'updated_at',
        )
        read_only_fields = ('id', 'size', 'size_readable', 'created_at', 'updated_at')