from rest_framework import viewsets, status
from rest_framework.decorators import api_view, permission_classes, list_route, detail_route
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .serializers import *
from .permissions import *


@api_view( ('GET','POST',) )
def project_list(request, format=None):

    if request.method == 'GET':
        projects = Project.objects.all().filter(user_id=request.user.id)
        serializer = ProjectSerializer(projects, many=True)
        return Response({'data': serializer.data})

    if request.method == 'POST':
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'data': serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view( ('GET','PUT','DELETE',) )
@permission_classes( (IsProjectOwner,) )
def projects_detail(request, project_id, format=None):
    try:
        project = Project.objects.get(pk=project_id)
    except Project.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProjectSerializer(project, many=False)
        return Response({'data': serializer.data})

    elif request.method == 'PUT':
        serializer = ProjectSerializer(project, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'data': serializer.data})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        project.delete()
        return Response({'detail': 'deleted'}, status=status.HTTP_204_NO_CONTENT)



@api_view( ('GET','POST',) )
@permission_classes( (IsProjectOwner,) )
def image_list(request, project_id, format=None):

    if request.method == 'GET':
        project = get_object_or_404(Project, id=project_id)
        images = Image.objects.all().filter(project_id=project.id)
        serializer = ImageSerializer(images, many=True)
        return Response({'data': serializer.data})

    if request.method == 'POST':
        serializer = ImageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view( ('GET','PUT','DELETE',) )
@permission_classes( (IsProjectOwner,) )
def image_detail(request, project_id, image_id, format=None):

    try:
        image = Image.objects.all().filter(project_id=project_id).get(id=image_id)
    except Image.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ImageSerializer(image, many=False)
        return Response({'data': serializer.data})

    elif request.method == 'PUT':
        serializer = ImageSerializer(image, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'data': serializer.data})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        image.delete()
        return Response({'detail': 'deleted'}, status=status.HTTP_204_NO_CONTENT)




@api_view( ('GET',) )
@permission_classes( (AllowAny,) )
def public_project(request, slug, format=None):
    try:
        project = Project.objects.get(slug=slug)
    except Project.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    try:
        image = Image.objects.all().filter(project_id=project.id)
    except Image.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ImageSerializer(image, many=True)
        return Response({'data': serializer.data})


@api_view( ('GET',) )
@permission_classes( (AllowAny,) )
def public_image(request, slug, format=None):
    try:
        image = Image.objects.get(slug=slug, active=True)
    except Image.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ImageSerializer(image, many=False)
        return Response({'data': serializer.data})