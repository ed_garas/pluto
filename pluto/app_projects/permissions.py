from rest_framework import permissions
from rest_framework.generics import get_object_or_404

from app_projects.models import Project, Image


class IsProjectOwner(permissions.BasePermission):
    message = 'Not found.'
    def has_permission(self, request, view):
        project = get_object_or_404(Project, id=view.kwargs['project_id'])
        return project.user_id == request.user.id


class IsProjectActive(permissions.BasePermission):
    message = 'Not found.'
    def has_permission(self, request, view):
        project = get_object_or_404(Project, id=view.kwargs['project_id'])
        return project.active


class IsImageActive(permissions.BasePermission):
    message = 'Not found.'
    def has_permission(self, request, view):
        image = get_object_or_404(Image, id=view.kwargs['slug'])
        return image.active


class IsMyProfile(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user