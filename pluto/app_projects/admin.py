from django.contrib import admin
from .models import *


class ImageAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'size', 'size_readable', 'screen_type', 'active')


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'images_count', 'space_readable', 'active')


admin.site.register(Project, ProjectAdmin)
admin.site.register(Image, ImageAdmin)
