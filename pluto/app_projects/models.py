from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
import string, random
from pluto_project.helpers import *


def user_avatar_path(instance, filename):
    return "user_{id}/avatar/{file}".format(id=instance.id, file=filename)

def project_images_path(instance, filename):
    return "user_{id}/project_{project}/{file}".format(id=instance.project.user.id, project=instance.project.id, file=filename)

def randomSlug():
    string_slug = string.ascii_lowercase + string.digits
    return ''.join(random.choice(string_slug) for i in range(random.randint(5,30)))



class Project(models.Model):
    name        = models.CharField(max_length=255)
    slug        = models.CharField(max_length=255, default=randomSlug)
    active      = models.BooleanField(default=False)
    user        = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)

    @property
    def images_count(self):
        return Image.objects.all().filter(project_id=self.id).count()

    @property
    def space(self):
        images = Image.objects.all().filter(project_id=self.id)
        space = 0
        for image in images:
            space += image.size
        return space

    @property
    def space_readable(self):
        return human_readable_size(self.space, 2)

    class Meta:
        verbose_name_plural = 'projects'

    def __str__(self):
        return self.name


class Image(models.Model):
    SCREEN_SIZES = (
        ('mobile', 'Mobile'),
        ('tablet', 'Tablet'),
        ('desktop', 'Desktop'),
    )

    name        = models.CharField(max_length=255, blank=True)
    slug        = models.CharField(max_length=255, default=randomSlug)
    image       = models.ImageField(upload_to=project_images_path)
    size        = models.IntegerField(blank=True, null=True)
    screen_type = models.CharField(max_length=10, blank=True, choices=SCREEN_SIZES)
    active      = models.BooleanField(default=False)
    project     = models.ForeignKey(Project, on_delete=models.CASCADE)
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if self.image.size:
            self.size = self.image.size

        super(Image, self).save(*args, **kwargs)

    @property
    def size_readable(self):
        return human_readable_size(self.size, 2)

    class Meta:
        verbose_name_plural = 'images'

    def __str__(self):
        return self.name