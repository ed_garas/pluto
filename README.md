# Pluto display - Django App

#### 1. Installing dependencies
```
sudo add-apt-repository ppa:jonathonf/python-3.6
```

```
sudo apt update
```

```
sudo apt install python3.6
```

```
sudo apt-get install build-essential libssl-dev libffi-dev
```

```
sudo apt-get install python-pip python-dev python-setuptools python3-pip python3-dev python3-setuptools
```

```
sudo apt-get install libpq-dev postgresql postgresql-contrib
```


#### 2. When dependencies and pip installed create virtual env:

```
mkdir ~/dev_folder && cd dev_folder
```

```
sudo pip install virtualenv
```

```
virtualenv -p python3.6 project_env
```

```
source project_env/bin/activate
```
#### 3. On "source project_env/bin/activate" terminal should have (project_env) before your name.this means that env is set up.


```
git clone PLUTO_DISTPLAY REPO
```

you should now see folders: bin, lib, pluto and requirements.txt file

```
pip install -r requirements.txt

```

#### 4. create Postgress database, I use DataGrip (app) for this.
#### 5. set up connection to database in 'pluto_project/settings.py'

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pluto_db',
        'USER': 'postgres',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

```

#### 6. make migration with:

```
python manage.py migrate

```